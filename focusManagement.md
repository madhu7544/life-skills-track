# Focus Management

## What is Deep Work

### 1) What is Deep Work?

Uninterrupted focus on work with zero distractions
This helps to build skills and produce work of impeccable quality

## Summary of Deep Work Book

### 2) Paraphrase all the ideas in the above videos and this one in detail.


* Intense periods of focus produce myelin in relevant areas of the brain, and it allows neurons to fire faster
* Ability to Deep Work is valuable and rare. Distractions happen every few minutes and break the flow of work which messes with our focus while working
* Distractions can be minimized by scheduling distractions
* Set boundaries
* Deep Work can be learnt. We can start it by scheduling it in phases and small durations of around 60 to 90 minutes
* Early morning is the best time to start deep working is in the morning when distractions are minimum
* Have shut-down rituals. Create an action plan for the next day


### 3) How can you implement the principles in your day-to-day life?

* Start with a small duration of 60 minutes
* Keep all distractions at bay during those 60 minutes
* Limit your use of social media and smartphones, which can distract you from your work and cause you to lose focus.
* Gradually increase the duration of deep work sessions
* Getting enough sleep




##  Dangers of Social Media

### 4) Your key takeaways from the video

* Distractions
* Unrealistic Expectations looking at others sharing life, truth or lie
* They are an entertainment product
* We are just products for ad agencies
* Designed to be addictive.
