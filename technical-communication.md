
# SQL Basics, Joins, and Aggregations

This document provides a comprehensive overview of SQL, covering the basics, joins, and aggregations. It includes code samples to illustrate each concept.

## Table of Contents
1 Introduction to SQL\
2 SQL Basics\
3 SQL Joins\
4 SQL Aggregations

## Introduction to SQL
SQL (Structured Query Language) is a programming language designed for managing and manipulating relational databases. It provides a standardized way to interact with databases and perform various operations, such as querying data, inserting, updating, and deleting records.

## SQL basics

1. Creating a Table
To store data in a database, you need to create a table. Each table consists of columns (fields) and rows (records). Here's an example of creating a "users" table:
```sql
CREATE TABLE users (
  id INT PRIMARY KEY,
  name VARCHAR(50),
  age INT,
  email VARCHAR(100)
);
```
2. Inserting Data
Once the table is created, you can insert data into it using the INSERT INTO statement:
```sql
INSERT INTO users (id, name, age, email)
VALUES (1, 'John Doe', 30, 'john@example.com');
```

3. Querying Data
SQL allows you to retrieve data from a table using the SELECT statement. You can specify which columns to include and apply conditions using the WHERE clause:
```sql
SELECT name, age FROM users WHERE age > 25;
```

4. Updating Data
To modify existing data, you can use the UPDATE statement. Specify the table, columns to update, and the new values. You can also use the WHERE clause to update specific rows:
```sql
UPDATE users SET age = 35 WHERE name = 'John Doe';
```

5. Deleting Data
To remove data from a table, you can use the DELETE FROM statement. Specify the table and use the WHERE clause to delete specific rows:
```sql
DELETE FROM users WHERE age > 40;
```
## SQL Joins
Joins allow you to combine rows from different tables based on related columns. They are essential for retrieving data that is spread across multiple tables. Here are a few types of joins commonly used:

1. Inner Join
The inner join returns only the matching rows from both tables:
```sql
SELECT orders.order_id, customers.customer_name
FROM orders
INNER JOIN customers ON orders.customer_id = customers.customer_id;
```

2. Left Join
The left join returns all rows from the left table and the matching rows from the right table:
```sql
SELECT customers.customer_name, orders.order_id
FROM customers
LEFT JOIN orders ON customers.customer_id = orders.customer_id;
```

3. Right Join
The right join returns all rows from the right table and the matching rows from the left table:
```sql
SELECT customers.customer_name, orders.order_id
FROM customers
RIGHT JOIN orders ON customers.customer_id = orders.customer_id;
```

4. Full Outer Join
The full outer join returns all rows when there is a match in either the left or right table:
```sql
SELECT customers.customer_name, orders.order_id
FROM customers
FULL OUTER JOIN orders ON customers.customer_id = orders.customer_id;
```
## Aggregations 
Aggregations in SQL are used to calculate summary statistics or perform calculations on a group of rows. Some commonly used aggregate functions include COUNT, SUM, AVG, MIN, and MAX.

1. COUNT
The COUNT function is used to count the number of rows that match a specific condition. The basic syntax of the COUNT function is as follows:
```sql
SELECT COUNT(column)
FROM table
WHERE condition;
```

2. SUM
The SUM function is used to calculate the sum of a numeric column. The basic syntax of the SUM function is as follows:
```sql
SELECT SUM(column)
FROM table
WHERE condition;
```
3. AVG
The AVG function is used to calculate the average value of a numeric column. The basic syntax of the AVG function is as follows:
```sql
SELECT AVG(column)
FROM table
WHERE condition;
```
4. MIN
The MIN function is used to find the minimum value of a column. The basic syntax of the MIN function is as follows:
```sql
SELECT MIN(column)
FROM table
WHERE condition;
```

5. MAX
The MAX function is used to find the maximum value of a column. The basic syntax of the MAX function is as follows:
```sql
SELECT MAX(column)
FROM table
WHERE condition;
```

## References

* W3Schools SQL Tutorial: The W3Schools website offers a comprehensive SQL tutorial with examples and interactive exercises.
    https://www.w3schools.com/sql/

* SQL for Data Analysis (Udacity): Udacity offers a free course called "SQL for Data Analysis" that covers SQL fundamentals, joins, and aggregations.
    https://www.udacity.com/course/sql-for-data-analysis--ud198

* Sql Tutorial for Beginners Youtube
    https://www.youtube.com/watch?v=On9eSN3F8w0&t=1652s&ab_channel=RishabhMishra
    
