# Listening and Assertive Communication #
## Active Listening ##

#### 1) What are the steps/strategies to do Active Listening? ####
Active Listening steps:
```
1. Avoid getting distracted by your own thoughts. Focus on the speaker and topic instead.
2. Try not to interrupt the other person. Let them finish and then respond.
3. Use door openers. These are phrases that show your are interested and keep the other person talking.
4. Show that you are listening with body language.
5. If appropriate take notes during important conversations.
6. Paraphrase what other have said to make sure you are on the same page. 
```
## Reflective Listening ##

#### 2) According to Fisher's model, what are the key points of Reflective Listening? ####

 Reflective listening is an essential aspect of Fisher's model, which aims to improve communication and understanding between individuals. Here are the key points of reflective listening:

```
1. Paying Attention
2. Empathy and understanding
3. Paraphrasing
4. Validation
5. Avoindg Judgment and Advice
```
## Reflection ##

#### 3) What are the obstacles in your listening process? ####

 External distractions, such as noise, Visual distrubances, Preconceived Ideas and Biases,Lack of Attention,Interrupting or Dominating the Conversation sometimes divert my attention away from the speaker's message.

 #### 4) What can you do to improve your listening? ####
 By practicing Active Listening, maintain eye contact, Avoid Interrupting, by allowing speaker to express without interruption, developing patience, taking notes.

 ## Types of Communication ##

 #### 5) When do you switch to Passive communication style in your day to day life? ####

1. Avoiding conflict: In certain situations, such as when dealing with aggressive or hostile individuals, adopting a passive communication style may help de-escalate the situation and prevent further conflict.


2. Respecting others' boundaries: If you perceive that someone does not wish to engage in a conversation or share personal information, you may choose to adopt a passive communication style out of respect for their boundaries.


3. Cultural considerations: In some cultures or social contexts, a more passive communication style is the norm and is considered respectful.


4. Receiving feedback: When receiving feedback or criticism, adopting a passive communication style can help you listen more openly and without becoming defensive.

#### 6) When do you switch into Aggressive communication styles in your day to day life? ####

When someone provoked me, I may respond with Aggressive communication, and sometimes I am frustrated, it goes emotionally.

#### 7) When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life? ####
 In situations when I feel threatened or attacked, I may resort to passive-aggressive behavior as a way to protect myself or regain control of the situation.

#### 8)How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? ####

1. Self-awareness: By understanding our own thoughts, feelings, and needs before engaging in a conversation.


2. Active listening: Practicing attentive listening, seeking to understand the perspectives and needs of others. This helps create an environment of mutual respect and facilitates effective communication.


3. Body Language: Using confident and open body language, maintaining appropriate eye contact, standing or sitting upright, and using gestures that convey attentiveness and engagement.


4. Respect boundaries: Recognizing and respecting the boundaries of others while maintaining our own.
