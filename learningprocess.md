 **1. How to Learn Faster with the Feynman Technique**


1. What is the Feynman Technique?

Ans: The Feynman Technique is a learning strategy that involves explaining complex concepts in simple terms to enhance understanding. It is named after Richard Feynman.

2. What are the different ways to implement this technique in your learning process?

Ans: Simplifing the concept by taking examples, diagrams, and real-world examples by explaining the concept to someone else and review the concept.

**2. Learning How to Learn TED talk by Barbara Oakley**

3. Summary of the Video

Ans: Barbara Oakley, talking about Focuced and diffuse modes of thinking, Memory, Habits, and test-taking.
```
*In the Focuced mode consentrate on a particular problem ot task.
*In the Diffucse mode Involve our thoughts.
*By using practice and repetedly doing the task can improve our memory.
*We can over come the procrastination by using strategies such as the Pomodoro technique.
*Test-taking and learning strategies are ways to optimize our performance.
```
4. What are some of the steps that you can take to improve your learning process?

Ans: To improve my learning strategies, I can find best resources and methods to learn that topic.I use online courses, books, videos
and according to that i plan and schedule and make a todo-list for what to learn.

**3. Learn Anything in 20 hours**

5. Summary and key takeaways from the video.

Ans: The common belief that it takes 10,000 hours to master a new talent is contested by Josh Kaufman. 
In his view, 20 hours of careful consentrate and practice.
*Key takeaways:
```
    * Deconstruct the skill into the smallest possible subskills
    * Learn enough to self-correct
    * Remove barriers to practice
    * Practice
```

6. What are some of the steps that you can while approaching a new topic?

Ans: Break the subject down into smaller, easier subtopics so i can learn them one at a time. In order to prevent information overload or misunderstanding, research the best resources and implement those.
