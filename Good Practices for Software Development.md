# Good Practices for Software Development

### 1)What is your one major takeaway from each one of the 6 sections. So 6 points in total.

* Make sure to ask questions and seek clarity in the meeting itself. Since most of the times, it will be difficult to get the same set of people online again.
* Implementation taking longer than usual due to some unexpected issue - Inform relevant team members
* Explain the problem clearly, mention the solutions you tried out to fix the problem
* Join the meetings 5-10 mins early to get some time with your team members
* Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.
* Programming is essentially a result of your sustained and concentrated attention


### 2)Which area do you think you need to improve on? What are your ideas to make progress in that area?

* Always communicate with everyone in our team, contribution and actively indulge in discussions. This builds a respectable professional rapport and naturally upgrades your performance at work.
* Have to manage my time as per learning. Without any interruptions.
* Checking my performance reviews. Identify where I excel and where i might need to focus to sharpen my skills.
* Be eager to learn asking seniors or employers questions and expressing your desire to collaborate with additional departments.
