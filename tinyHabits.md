# Tiny Habits

## 1. Tiny Habits - BJ Fogg
#### 1. Your takeaways from the video (Minimum 5 points)
* The speaker introduces the concept of "tiny habits" as a powerful technique for facilitating behavior change by making small adjustments.
* The audience actively engages in a "tiny habit" exercise where they are asked to floss one tooth and celebrate the immediate victory, demonstrating the effectiveness of small actions.
* The speaker simplifies behavior change by explaining that there are only 15 ways behaviors can change, with five of them resulting in long-term transformations.
* The speaker highlights the limitations of relying solely on motivation and willpower for sustained behavior change, suggesting that incorporating tiny habits into daily routines is a more effective approach.
* Tiny habits are emphasized as easily integratable behaviors that can support individuals in achieving their goals without constantly depending on motivation or willpower.

## 2. Tiny Habits by BJ Fogg - Core Message

#### 2. Your takeaways from the video in as much detail as possible
* The Universal Formula for human behavior is expressed as B = MAP, where B represents Behavior, M is Motivation, A stands for Ability, and P denotes Prompt.
* According to the formula, a person will only engage in a specific behavior if their motivation and ability align with the requirements of that behavior, and if a prompt is present.
* The speaker suggests shrinking behaviors by finding the smallest possible version of the desired habit, one that can be completed within 30 seconds or less.
* For instance, the tiniest workout could involve performing just one push-up, one squat, or one yoga pose.
* Breaking down the behavior into its smallest form makes it more manageable and easier to incorporate into daily routines.

#### 3. How can you use B = MAP to make making new habits easier?
The B=MAP framework, where Behavior equals Motivation + Ability + Prompt, offers a method to facilitate the formation of new habits. Here's how it can be applied:

* Motivation: Boost your motivation for the desired habit by identifying a compelling reason behind it.
* Ability: Simplify the habit by breaking it down into small, achievable steps, making it easier to incorporate into your routine.
* Prompt: Establish cues or prompts that serve as reminders to perform the habit. Examples include setting reminders on your phone or placing notes in visible areas around your home.

By following these principles, individuals can enhance their motivation, make habits more manageable, and create effective prompts to reinforce consistent behavior change.

#### 4. Why it is important to "Shine" or Celebrate after each successful completion of habit?
* Celebrating each successful completion of a habit is vital as it reinforces positive behavior and provides motivation to continue. By acknowledging your efforts and creating a sense of accomplishment, celebrating your successes boosts your mood and reinforces the habit further.


## 3. 1% Better Every Day Video

#### 5. Your takeaways from the video (Minimum 5 points)
* James Clear, the speaker, discusses the concept of "1% better every day" emphasizing the power of small, consistent improvements over time.
* The video was recorded at the ConvertKit Craft + Commerce conference in 2017, targeting a creative and entrepreneurial audience.
* The talk likely centers around personal development and self-improvement, applicable to various areas of life such as health, productivity, and relationships.
* Clear emphasizes the importance of habits and routines in achieving sustained progress, encouraging the establishment of daily rituals and small habits to build momentum.
* The concept emphasizes embracing a growth mindset, focusing on the process of improvement rather than immediate outcomes, fostering long-term success and enjoyment of the journey.

## 4. Book Summary of Atomic Habits

#### 6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

* The book highlights the importance of identity, processes, and outcomes in habit formation, suggesting that habits are formed through cues, routines, and rewards, and changing habits involves a shift in self-perception.
* It recommends setting SMART goals, tracking progress, and cultivating a growth mindset to stay motivated and effectively develop new habits.
* The book also emphasizes the significance of practicing self-compassion when facing setbacks during the habit formation process.

#### 7. Write about the book's perspective on how to make a good habit easier?
* "Atomic Habits" by James Clear advises simplifying good habits by breaking them down into smaller, manageable steps and emphasizing consistency in their practice.
* The book also recommends utilizing habit stacking, where new habits are built upon existing routines, and associating positive feelings or rewards with the desired habits to reinforce their adoption.

#### 8. Write about the book's perspective on making a bad habit more difficult?
* The book proposes reducing the appeal or convenience of a bad habit by increasing friction, making it less attractive or convenient to engage in. It also suggests introducing obstacles or "speed bumps" to make it more challenging to carry out the habit, discouraging its repetition.

## 5. Reflection

#### 9.  Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying? 
* Practice yoga for 15-20 minutes each day for the next two weeks.
* Attend at least two yoga classes at a local studio or online per week for the next month.
* Learn and practice three new yoga poses each week for the next month.
* Incorporate meditation or breathwork into your yoga practice for 7-10 minutes each day for the next two weeks.

#### 10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
* Make the cue invisible: Remove the phone from your immediate environment and turn off non-essential notifications and alerts.
* Make the process unattractive: Reduce social media use, unfollow unhelpful accounts, set time limits, and turn on grayscale mode.
* Make the response unsatisfying: Set consequences for yourself, find alternative activities that you enjoy more than using your phone.

