
# Energy Management

## 1. Manage Energy not Time

#### 1) What are the activities you do that make you relax - Calm quadrant ?

* A brief period of meditation has helped me relax and calm my mind.


* Every evening, I go for a walk to warm up and to clear my mind of the numerous thoughts that race through it.


* I can lower my stress and concentrate on things that are more important by having good conversations with my family and friends.


* I hear my favourite music, which calms my mind.

#### 2) When do you find getting into the Stress quadrant?


* If I am capable to do some tasks and not properly doing them.

* When some bad things happen unexpectedly, which not should be.

* When I do not complete tasks on time.

#### 3) How do you understand if you are in the Excitement quadrant?
---

I get really excited to go on outings with my friends. I'm glad when I finish my task ahead of schedule and have time to work on other things.

* Food that tastes excellent makes me happy.


* I get excited when I reach the goals I've set for my life.


* Travelling to new places makes me feel excited.

* I feel fulfilled when I work on creative projects and learn new things.

## 2. Sleep is your superpower

#### 4) Paraphrase the Sleep is your Superpower video in detail.

* Both before and after learning anything, sleep is crucial to the learning process. Prior to learning something new, getting enough sleep prepares our brain to take in more information.

* The speaker's research indicates that those with regular sleep patterns are 40% more productive when learning.

* Limiting our sleep to 4 hours can result in a 75% decrease in natural killer cells, which in fact help our body resist sickness.

* Establishing a regular sleep routine can improve our sleep.

* Maintaining a low body temperature as well as the ambient temperature can promote rapid sleep.

#### 5) What are some ideas that you can implement to sleep better?
---

* I can go to sleep earlier if I don't use my phone right before bed.

* I can get good quality sleep if I set a regular time to go to bed.

* Going to bed early in order to get more than six hours of sleep.

* Minimising my caffeine consumption.

* Regularly practising meditation and prayer.

* Cutting back on eating before bed.

## 3. Brain Changing Benefits of Exercise

#### 6) Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
---

* She started working on her health. and started exercising regularly; she also tried yoga, Zumba, kickboxing, and every other physical activity that could help her achieve her health goals.

* At first, it was tough for her, but later she figured out that all these physical activities boosted her mood and encouraged her to do more. and ultimately motivated her to stay focused on achieving her health goals.

* Then she shared that her efficacy in grant writing increased as she was able to focus better than before.

* According to literature, Excersing helps us get better moods, better energy,  better memory, and better attention, and she herself experienced all these changes in herself.



#### 7) What are some steps you can take to exercise more?

* Attempt to be more physically active and complete all of my daily tasks on my own, without assistance.

* Making at least 15 to 20 minutes of exercise a part of your daily schedule.

* When travelling small distances, prefer to walk rather than use a vehicle.

* Exercising with a friend can help keep me motivated and make my workouts more enjoyable.
