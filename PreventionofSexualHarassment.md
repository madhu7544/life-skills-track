# Prevention of Sexual Harassment

## 1) What kinds of behavior cause sexual harassment?

___
Sexual harassment is any unwelcome verbal visual or physical conduct of sexual nature that affect working conditions or creates a hostile environment.

There are 3 forms of sexual harassment

1. Verbal
2. Visual
3. Sexual

**Causes of sexual harassment**

1. Comments about clothing, Gender-based jokes, Sexual jokes or comments and requesting sexual favors.
2. Obscene postures drawing or pictures texts or emails
3. Sexual assault, impeding or blocking movement, inappropriate touch like kissing, hugging or rubbing.
4. Circulating photos of pornographic nature.
5. Interrogating an employee about their sex life.
6. Leaving unwanted gifts of a sexual or romantic nature.
7. Spreading sexual rumors about a colleague.
***
## 2) What would you do in case you face or witness any incident or repeated incidents of such behavior?

If I will face or witness any incident or repeated incidents of sexual harassment behavior, I follow the steps below.

1. Keep a record of what happened, including the time, location, and details of the incident. It's important to have a clear and concise account of the events.
2. Report the incident to a manager, supervisor, or HR representative. If the perpetrator is a coworker, report it to your manager or HR department. If the perpetrator is a customer or client, report it to your supervisor.
3. Take support from friends, and family. Talking to someone can help alleviate the stress and anxiety that may come with experiencing sexual harassment.
4. Be familiar with the company's policies on sexual harassment, as well as your legal rights. It's important to know what options are available to you and what steps you can take to protect yourself.
5. Consider taking action to prevent future incidents. This may involve speaking out against the behavior, supporting policies that promote a safe and respectful environment or offering resources to those affected.

