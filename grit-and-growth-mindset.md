## 1.Grit

#### 1) Paraphrase (summarize) the video in a few lines. Use your own words.
Ans. This video explains that being smart, as measured by IQ, doesn't guarantee success in school. What truly matters is having "grit," which means having a strong passion for learning and not giving up easily, even when faced with challenges. Students with more grit are more likely to finish school and perform well. The video highlights the need for further research to discover effective ways to help children develop grit and achieve success in their academic journey.

#### 2) What are your key takeaways from the video to take action on?
Ans: 
* Learn from mistakes and keep trying: Don't give up easily. Find different ways to develop a passion for what you're learning and persistently work towards your goals.

* Motivation and mindset matter: Education should consider how to keep students motivated and help them develop a positive attitude towards learning. It's important to understand the psychological aspect of education.

* Your ability to learn can improve: Remember, your learning ability is not fixed. It can get better with effort and hard work. Don't be discouraged by challenges; keep trying and you'll see improvement.



## 2. Introduction to Growth Mindset


#### 3) Paraphrase (summarize) the video in a few lines in your own words.
Ans. The video introduces two mindsets: fixed and growth mindsets. A fixed mindset believes abilities are fixed, leading to avoidance of challenges and feedback. In contrast, a growth mindset believes abilities can improve through effort, embracing challenges and feedback as learning opportunities. Mindsets can vary and change, and understanding their qualities helps individuals develop a growth mindset, fostering a willingness to learn and grow.

#### 4) What are your key takeaways from the video to take action on?
Ans. 
* Growth Mindset: The video emphasizes the importance of having a growth mindset, which believes in the potential for development and improvement of skills over time.

* Characteristics of Mindsets: A growth mindset focuses on the process of learning, embraces challenges, mistakes, and feedback as opportunities for growth, and has confidence in the ability to learn and develop.

* Four Key Ingredients to Growth: Effort, challenges, mistakes, and feedback are identified as crucial elements that contribute to growth and learning.

* Creating a Culture for Learning: To foster a culture of learning, it is important to promote a growth mindset, value effort, encourage learning from mistakes, and provide constructive feedback.

* By adopting a growth mindset and incorporating these principles, individuals can unleash their potential, overcome obstacles, and continuously improve their abilities.

## 3. Understanding Internal Locus of Control


#### 5) What is the Internal Locus of Control? What is the key point in the video?
Ans:
* Internal Locus of Control: It is the belief that you have control over your life and that your actions and efforts impact your outcomes.

* Importance: Having an internal locus of control is crucial for motivation, as it means taking responsibility for your actions and believing that hard work leads to success.

* Study Findings: Research shows that individuals with an internal locus of control exhibit higher motivation and perseverance compared to those who attribute success to external factors.

* Benefits: Adopting an internal locus of control empowers individuals, fostering motivation, a sense of control, and a belief in personal effort as a driver of success.

By embracing an internal locus of control, individuals can feel empowered, work hard, and persist in the face of challenges, leading to greater motivation and belief in their ability to achieve their goals.

## 3. How to build a Growth Mindset

#### 6) Paraphrase (summarize) the video in a few lines in your own words.

Ans:
* A growth mindset begins with the fundamental rule of life, which is believing in our ability to  figure things out. If we believe that we can figure things out and get better, it enables us to pursue lifelong improvement and growth.
* The second thing to do to develop a greater growth mindset is to question our assumptions. 
* Don't let your current knowledge, skills, and abilities box in or narrow down your vision for the future because today has little to do with what you're capable of achieving in the future.





#### 7) What are your key takeaways from the video to take action on?

Ans:

* Believe in your ability to figure things out: Embrace the belief that you have the capacity to learn, improve, and overcome challenges.
* Question your assumptions: Challenge any fixed beliefs or assumptions that may be holding you back.
* Create your own learning curriculum: Take responsibility for your personal growth by designing your own learning path.
* Embrace the struggle: Instead of shying away from challenges or setbacks, view them as opportunities for growth.
* Cultivate resilience: Develop resilience by maintaining a positive attitude and persevering in the face of difficulties.
* Seek feedback and learn from criticism: Embrace feedback as a valuable tool for growth.

## 4. Mindset - A MountBlue Warrior Reference Manual


#### 8) What are one or more points that you want to take action on from the manual? (Maximum 3)

Ans: Points that I want to take action on from the manual are:

* I am 100 percent responsible for my learning.
* I will be focused on mastery till I can do things half asleep. If someone wakes me up at 3 AM in the night, I will calmly solve the problem like James Bond and go to sleep.
* I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.
